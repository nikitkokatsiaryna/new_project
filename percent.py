proz = int(input()) #17
rub = int(input())  #94
kop = int(input())  #41

amount_percent = rub * (proz / 100)

amount_percent_rub = amount_percent // 1
amount_percent_kop = (amount_percent % 1) * 100

next_year_rub = round(rub + amount_percent_rub)
next_year_kop = round(kop + amount_percent_kop)

if next_year_kop >= 100:
    plus_rub = next_year_kop // 100
    next_year_rub += plus_rub
    next_year_kop = next_year_kop % 100

print(str(next_year_rub) + ' ' + str(next_year_kop))
